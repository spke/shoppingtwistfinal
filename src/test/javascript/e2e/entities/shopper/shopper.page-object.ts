import { element, by, ElementFinder } from 'protractor';

export class ShopperComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-shopper div table .btn-danger'));
  title = element.all(by.css('jhi-shopper div h2#page-heading span')).first();

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getText();
  }
}

export class ShopperUpdatePage {
  pageTitle = element(by.id('jhi-shopper-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  displayNameInput = element(by.id('field_displayName'));
  phoneNumberInput = element(by.id('field_phoneNumber'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }

  async setDisplayNameInput(displayName: string): Promise<void> {
    await this.displayNameInput.sendKeys(displayName);
  }

  async getDisplayNameInput(): Promise<string> {
    return await this.displayNameInput.getAttribute('value');
  }

  async setPhoneNumberInput(phoneNumber: string): Promise<void> {
    await this.phoneNumberInput.sendKeys(phoneNumber);
  }

  async getPhoneNumberInput(): Promise<string> {
    return await this.phoneNumberInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ShopperDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-shopper-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-shopper'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
