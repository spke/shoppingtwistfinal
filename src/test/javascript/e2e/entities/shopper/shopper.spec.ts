import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ShopperComponentsPage, ShopperDeleteDialog, ShopperUpdatePage } from './shopper.page-object';

const expect = chai.expect;

describe('Shopper e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let shopperComponentsPage: ShopperComponentsPage;
  let shopperUpdatePage: ShopperUpdatePage;
  let shopperDeleteDialog: ShopperDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Shoppers', async () => {
    await navBarPage.goToEntity('shopper');
    shopperComponentsPage = new ShopperComponentsPage();
    await browser.wait(ec.visibilityOf(shopperComponentsPage.title), 5000);
    expect(await shopperComponentsPage.getTitle()).to.eq('Shoppers');
  });

  it('should load create Shopper page', async () => {
    await shopperComponentsPage.clickOnCreateButton();
    shopperUpdatePage = new ShopperUpdatePage();
    expect(await shopperUpdatePage.getPageTitle()).to.eq('Create or edit a Shopper');
    await shopperUpdatePage.cancel();
  });

  it('should create and save Shoppers', async () => {
    const nbButtonsBeforeCreate = await shopperComponentsPage.countDeleteButtons();

    await shopperComponentsPage.clickOnCreateButton();
    await promise.all([shopperUpdatePage.setDisplayNameInput('displayName'), shopperUpdatePage.setPhoneNumberInput('phoneNumber')]);
    expect(await shopperUpdatePage.getDisplayNameInput()).to.eq('displayName', 'Expected DisplayName value to be equals to displayName');
    expect(await shopperUpdatePage.getPhoneNumberInput()).to.eq('phoneNumber', 'Expected PhoneNumber value to be equals to phoneNumber');
    await shopperUpdatePage.save();
    expect(await shopperUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await shopperComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Shopper', async () => {
    const nbButtonsBeforeDelete = await shopperComponentsPage.countDeleteButtons();
    await shopperComponentsPage.clickOnLastDeleteButton();

    shopperDeleteDialog = new ShopperDeleteDialog();
    expect(await shopperDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Shopper?');
    await shopperDeleteDialog.clickOnConfirmButton();

    expect(await shopperComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
