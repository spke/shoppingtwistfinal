import { element, by, ElementFinder } from 'protractor';

export class ShoppingListComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-shopping-list div table .btn-danger'));
  title = element.all(by.css('jhi-shopping-list div h2#page-heading span')).first();

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getText();
  }
}

export class ShoppingListUpdatePage {
  pageTitle = element(by.id('jhi-shopping-list-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  nameInput = element(by.id('field_name'));
  categorySelect = element(by.id('field_category'));
  shopperSelect = element(by.id('field_shopper'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async categorySelectLastOption(): Promise<void> {
    await this.categorySelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async categorySelectOption(option: string): Promise<void> {
    await this.categorySelect.sendKeys(option);
  }

  getCategorySelect(): ElementFinder {
    return this.categorySelect;
  }

  async getCategorySelectedOption(): Promise<string> {
    return await this.categorySelect.element(by.css('option:checked')).getText();
  }

  async shopperSelectLastOption(): Promise<void> {
    await this.shopperSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async shopperSelectOption(option: string): Promise<void> {
    await this.shopperSelect.sendKeys(option);
  }

  getShopperSelect(): ElementFinder {
    return this.shopperSelect;
  }

  async getShopperSelectedOption(): Promise<string> {
    return await this.shopperSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ShoppingListDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-shoppingList-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-shoppingList'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
